package xyz.james.contentproviderex;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button mAddView, mRetrieveView;
    private EditText mNameView, mScoreView, mRankView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAddView = (Button) findViewById(R.id.btnAdd);
        mRetrieveView = (Button) findViewById(R.id.btnRetrieve);
        mNameView = (EditText) findViewById(R.id.txtName);
        mScoreView = (EditText) findViewById(R.id.txtScore);
        mRankView = (EditText) findViewById(R.id.txtRank);

        mAddView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ContentValues values = new ContentValues();
                values.put(TruongBeoProvider.NAME, mNameView.getText().toString());
                values.put(TruongBeoProvider.SCORE, mScoreView.getText().toString());
                values.put(TruongBeoProvider.RANK, mRankView.getText().toString());

                Uri uri = getContentResolver().insert(TruongBeoProvider.CONTENT_URI, values);
                Toast.makeText(getApplicationContext(), uri.toString(), Toast.LENGTH_LONG).show();
            }
        });

        mRetrieveView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String URI = TruongBeoProvider.URI;
                Uri staff = Uri.parse(URI);
                Cursor c = getContentResolver().query(staff, null, null, null, TruongBeoProvider.NAME);
                if (c == null) return;

                StringBuilder builder = new StringBuilder();
                if (c.moveToFirst()) {
                    do {
                        builder.append(c.getInt(c.getColumnIndex(TruongBeoProvider._ID)))
                                .append(" ")
                                .append(c.getString(c.getColumnIndex(TruongBeoProvider.NAME)))
                                .append(" ")
                                .append(c.getString(c.getColumnIndex(TruongBeoProvider.SCORE)))
                                .append(" ")
                                .append(c.getString(c.getColumnIndex(TruongBeoProvider.RANK)))
                                .append("\n");

                    } while (c.moveToNext());
                }

                Toast.makeText(getApplicationContext(), builder.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
