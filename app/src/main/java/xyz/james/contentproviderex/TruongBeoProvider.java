package xyz.james.contentproviderex;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.HashMap;

/**
 * Created by TruongVN on 06/10/2016.
 */
public class TruongBeoProvider extends ContentProvider {

    static final String PROVIDER_NAME = "com.truongbeo.provider";
    static final String URI = "content://" + PROVIDER_NAME + "/staff";
    static final Uri CONTENT_URI = Uri.parse(URI);

    static final String _ID = "_id";
    static final String NAME = "name";
    static final String SCORE = "score";
    static final String RANK = "rank";

    private static HashMap<String, String> STAFF_PROJECTION_MAP;

    static final int STAFF = 1;
    static final int STAFF_ID = 2;

    static final UriMatcher uriMatcher;

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(PROVIDER_NAME, "staff", STAFF);
        uriMatcher.addURI(PROVIDER_NAME, "staff/#", STAFF_ID);
    }

    private SQLiteDatabase database;
    static final String DATABASE_NAME = "NTQ_Solution";
    static final String STAFF_TABLE_NAME = "staff";
    static final int DATABASE_VERSION = 1;
    static final String CREATE_DB_TABLE =
            "CREATE TABLE " + STAFF_TABLE_NAME +
                    " (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "name TEXT NOT NULL,  " +
                    "score TEXT NOT NULL, " +
                    "rank TEXT NOT NULL);";


    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {
            sqLiteDatabase.execSQL(CREATE_DB_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXIST " + STAFF_TABLE_NAME);
            onCreate(sqLiteDatabase);
        }
    }

    @Override
    public boolean onCreate() {
        DatabaseHelper dbHelper = new DatabaseHelper(getContext());
        database = dbHelper.getReadableDatabase();
        return database != null;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] strings, String s, String[] strings1, String s1) {
        //strings - projection, s - selection, strings1 selectionArgs, s1 - sortOrder
        SQLiteQueryBuilder sqLiteQueryBuilder = new SQLiteQueryBuilder();
        sqLiteQueryBuilder.setTables(STAFF_TABLE_NAME);

        switch (uriMatcher.match(uri)) {
            case STAFF:
                sqLiteQueryBuilder.setProjectionMap(STAFF_PROJECTION_MAP);
                break;
            case STAFF_ID:
                sqLiteQueryBuilder.appendWhere(_ID + "=" + uri.getPathSegments().get(1));
                break;
            default:
                throw new IllegalArgumentException("Unknow URI: " + uri);
        }

        if (TextUtils.isEmpty(s1)) {
            s1 = NAME;
        }
        Cursor c = null;
        try {
            c = sqLiteQueryBuilder.query(database, strings, s, strings1, null, null, s1);
            c.setNotificationUri(getContext().getContentResolver(), uri);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uriMatcher.match(uri)) {
            case STAFF:
                return "vnd.android.cursor.dir/vnd.truongbeo.staff";
            case STAFF_ID:
                return "vnd.android.cursor.item/vnd.truongbeo.staff";
            default:
                throw new IllegalArgumentException("Unsupport URIL: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues contentValues) {
        long rowId = database.insert(STAFF_TABLE_NAME, "", contentValues);
        if (rowId > 0) {
            Uri localUri = ContentUris.withAppendedId(CONTENT_URI, rowId);
            getContext().getContentResolver().notifyChange(localUri, null);
            return localUri;
        }
        throw new SQLException("Failed to add to a record into " + uri);
    }

    @Override
    public int delete(@NonNull Uri uri, String s, String[] strings) {
        //s - selection, strings - selectionArguments
        int count = 0;

        switch (uriMatcher.match(uri)) {
            case STAFF:
                count = database.delete(STAFF_TABLE_NAME, s, strings);
                break;

            case STAFF_ID:
                count = database.delete(STAFF_TABLE_NAME, _ID + "=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(s) ? " AND (" + s + ')' : ""), strings);
                break;
            default:
                throw new IllegalArgumentException("Unknow URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues contentValues, String s, String[] strings) {
        //s - selection, strings - selectionArgs
        int count = 0;

        switch (uriMatcher.match(uri)) {
            case STAFF:
                count = database.update(STAFF_TABLE_NAME, contentValues, s, strings);
                break;

            case STAFF_ID:
                count = database.update(STAFF_TABLE_NAME, contentValues, _ID + "=" + uri.getPathSegments().get(1) + (!TextUtils.isEmpty(s) ? " AND (" + s + ')' : ""), strings);
                break;
            default:
                throw new IllegalArgumentException("Unknow URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
